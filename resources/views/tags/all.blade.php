@extends('layout')

@section('title', 'Tags')

@section('content')

    <div id="page" class="container center-align">
        @if($message = Session::get('message'))
            <h4 class="text-accent-4 green-text">{{ $message }}</h4>
        @endif

        <div id="content">
            <div class="title">
                <h5>Create Tags from here!</h5>
            </div>
                <a class="waves-effect waves-green btn" href="{{url('/tags/create')}}">
                    Create
                </a>
        </div>
        <div id="sidebar">

            <h4>All Tags</h4>
            <hr>
            @foreach($tags as $tag)
            <ul style="background-color: #fff7c3">
               <a href="{{url('/tags/edit/'.$tag->name)}}">
                   <li>{{$tag->name}}</li>
               </a>
            </ul>
            @endforeach

            <div class="">
                {{$tags->links()}}
            </div>

        </div>
    </div>

@endsection
