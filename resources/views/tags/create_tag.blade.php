@extends('layout')

@section('title', 'Create New Tag')

@section('content')
    <div id="page" class="container">

        <div class="container center-align">
            @if($message = Session::get('message'))
                <h4 class="text-accent-4 green-text">{{ $message }}</h4>
            @endif
            <h4>New Tag</h4>
            <form method="POST" action="{{url('/tags')}}" class="col s6 ">
                @csrf
                <div class="row">
                    <div class="input-field col s3 offset-s3 offset-l5">
                        <input id="input_text" class="@error('name') materialize-red lighten-4 @enderror" type="text" name="name" class="validate" >
                        <label for="input_text">Name</label>

                        @error('name')
                        <span class="helper-text materialize-red-text" data-error="wrong" data-success="right">{{$errors->first('name')}}</span>
                        @enderror
                    </div>
                </div>

                <div class="input-field col s6">
                    <button class="btn waves-effect waves-light" type="submit">Save
                    </button>

                </div>

            </form>
        </div>

    </div>
@endsection
