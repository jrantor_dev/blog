@extends('layout')

@section('title', 'Edit Tag')

@section('content')
    <div id="page" class="container">

        <div class="container center-align">

            <h4>Edit Tag</h4>
            <form method="POST" action="{{url('/tags/'.$tag->id)}}" class="col s6 ">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="input-field col s3 offset-s3 offset-l5">
                        <input id="input_text" class="@error('name') materialize-red lighten-4 @enderror" type="text" name="name" value="{{$tag->name}}" class="validate" >
                        <label for="input_text">Name</label>

                        @error('name')
                        <span class="helper-text materialize-red-text" data-error="wrong" data-success="right">{{$errors->first('name')}}</span>
                        @enderror
                    </div>
                </div>

                <div class="input-field col s6">
                    <button class="btn waves-effect waves-light" type="submit">Save
                    </button>
                    <a href="{{url('/tags/'.$tag->id.'/delete')}}" class="waves-effect waves-light btn materialize-red"
                       onclick="return confirm('DELETE TAG?');" style="margin-left: 8px;">
                        <i class="material-icons">delete</i>
                    </a>
                </div>

            </form>

        </div>

    </div>
@endsection
