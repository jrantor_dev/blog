@extends('layout')

@section('title','Edit Article')

@section('content')
<div id="page" class="container ">

	<div class="center-align" >

		<h4>Update Article</h4>
	<div class="row">
      <form  action="{{url('/articles')}}/{{$article->id}}" method="POST" class="col s6"  >
        @csrf
        @method('PUT')
        <div class="row">
          <div class="input-field col s12 offset-l6">
            <input id="input_text" class="@error('title') materialize-red lighten-4 @enderror" type="text" name="title" value="{{$article->title}}">
            <label for="input_text">Title</label>

            @error('title')
            <span class="helper-text materialize-red-text" data-error="wrong" data-success="right">{{$errors->first('title')}}
            </span>
            @enderror
          </div>
        </div>

          <div class="row">
              <div class="input-field col s12 offset-l6">
                  <input id="input_text" class="@error('slug') materialize-red lighten-4 @enderror" type="text" name="slug" value="{{$article->slug}}">
                  <label for="input_text">Slug</label>

                  @error('slug')
                  <span class="helper-text materialize-red-text" data-error="wrong" data-success="right">{{$errors->first('slug')}}
            </span>
                  @enderror
              </div>
          </div>

        <div class="row">
          <div class="input-field col s12 offset-l6">
            <textarea id="textarea1" class="materialize-textarea @error('excerpt') materialize-red lighten-4 @enderror" name="excerpt" >{{$article->excerpt}}</textarea>
            <label for="textarea1">Excerpt</label>

            @error('excerpt')
            <span class="helper-text materialize-red-text" data-error="wrong" data-success="right">{{$errors->first('excerpt')}}
            </span>
            @enderror


          </div>
        </div>

          <div class="row">
              <div class="input-field col s12 offset-l6">
                  <span>Select Tags</span>
                  <select name="category_id" id="categories">
                      <option>Choose</option>
                      @foreach($categories as $category)
                          <option value="{{$category->id}}">{{$category->category_name}}</option>
                      @endforeach
                  </select>

              </div>
          </div>

          <div class="row">
              <div class="input-field col s12 offset-l6">
                  <span>Select Tags</span>
                  <select multiple name="tags[]" id="tags">
                      @foreach($tags as $tag)
                          <option value="{{$tag->id}}">{{$tag->name}}</option>
                      @endforeach
                  </select>

              </div>
          </div>

        <div class="row">
          <div class="input-field col s12 offset-l6">
            <textarea id="textarea1" class="ckeditor materialize-textarea @error('body') materialize-red lighten-4 @enderror" name="body" >{{$article->body}}</textarea>
            <label for="textarea1">Description</label>

            @error('body')
            <span class="helper-text materialize-red-text" data-error="wrong" data-success="right">{{$errors->first('body')}}
            </span>
            @enderror
          </div>
        </div>

      <div class="input-field col s6 offset-l8">
        <button class="btn waves-effect waves-light" type="submit">Submit
	    <i class="material-icons right">send</i>
	     </button>

      </div>

	</form>
	</div>
</div>
</div>
@endsection

@section('scripts')
<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
    <script>
        $(document).ready(function(){
            $('#categories').formSelect().val({{json_encode($article->category_id)}}).trigger('change');
            $('#categories').formSelect();
            $('#tags').formSelect().val({{json_encode($article->tags()->allRelatedIds())}}).trigger('change');
            $('#tags').formSelect();

            $('.ckeditor').ckeditor();
        });
    </script>
@endsection
