@extends('layout')

@section('title','Articles')

@section('content')
<div id="page">
    <div class="container" id="content">
        @if($message = Session::get('success'))
            <h4 class="text-accent-4 green-text center-align">{{ $message }}</h4>
        @endif
        
        @foreach($articles as $article)
            <div id="content" class="in_article">
                    <a href="{{url('/articles/'.$article->slug)}}"><h3>{{$article->title}}</h3></a>
                    @guest
                    <form method="POST" action="{{url('/article/favorite/'.$article->id)}}">
                        @csrf
                    <button class=" btn btn-flat right blue-grey lighten-2" type="submit">
                        <i class="material-icons">favorite_border</i>{{App\Http\Controllers\FavoriteController::show($article)}}
                    </button>
                    </form>
                    @endguest
                    @auth
                    @if(App\Article::isFavorite($article))
                
                    <a href="{{url('/article/rm_favorite/'.$article->id)}}" class=" btn btn-flat right pink accent-4" onclick="return confirm('Remove from favorites?');">
                        <i class="material-icons">favorite</i>{{App\Http\Controllers\FavoriteController::show($article)}}
                    </a>
                    @else
                    <form method="POST" action="{{url('/article/favorite/'.$article->id)}}">
                        @csrf
                    <button class=" btn btn-flat right blue-grey lighten-2" type="submit">
                        <i class="material-icons">favorite_border</i>{{App\Http\Controllers\FavoriteController::show($article)}}
                    </button>
                    </form>
                    @endif
                    @endauth
                   <div class="art_meta">
                  <i class="material-icons">visibility</i><sup>13k views</sup>
                  <span><sup> 2 min read | 5 comments </sup></span> 
                </div>
                <br>
                <p><img src="{{asset('images/banner.jpg')}}" alt="" class="image image-full" /> </p>
                <a href="{{url('/articles/'.$article->slug)}}">
                <h5 class="byline">{{$article->excerpt}}</h5>
                    <p style="font-weight: bold;">Read More...</p>
                </a>
                <hr>

                published
                    <p class="tooltip">
                        {{$article->created_at->diffForHumans()}}
                        <span class="tooltiptext"> {{$article->created_at->toDayDateTimeString()}}</span>
                    </p>
                    by <b> {{$article->user->name}} </b>
                
            </div>

        @endforeach
    </div>

    <div class="container">
        @include('articles.sidebar')
    </div>

	</div>
		{{$articles->links()}}

@endsection

@section('scripts')
<script>
    const borders = ['art_border','art_border1','art_border2'];
    const articles = document.querySelectorAll('.in_article');
    const rndnum = getRandomNum();
    articles.forEach(article => article.classList.add(borders[rndnum]));
    function getRandomNum(){
        return Math.floor(Math.random()*borders.length);
    }
</script>
@endsection