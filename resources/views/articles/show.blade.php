@extends('layout')

@section('title',$article->title)

@section('content')

<div id="page" class="container" style="padding-left: 240px;">
		<div id="content">
      
			<div class="title row">
				<h2 class="single-title">{{$article->title}}</h2>
       <br>
				<p class="tooltip" style="font-size: 1.2rem;">Posted {{$article->created_at->diffForHumans()}}
					<span class="tooltiptext">{{$article->created_at->toDayDateTimeString()}}</span>
				</p>
            <span style="font-size: 1.8rem; margin: 2px; font-weight: bold;">>_</span> <a href="/articles/categories/{{$category->category_name}}" class="btn waves-effect waves-light"><em><b>{{$category->category_name}}</b></em></a>

        <p style="font-size: 1.2rem;">Last Updated {{$article->updated_at->toDayDateTimeString()}}</p>        
				<br>
        <img src="../images/user.jpg" alt="avatar" width="50" height="50"> 
        <a class="user_name" href="#">{{$author->name}}</a>

        <h5>
				<span class="byline">{{$article->excerpt}}</span> </h5>
			</div>
      
			<p><img src="../images/banner.jpg" alt="" class="image image-full" /> </p>
           <article>
               <p class="article_description"> {!! $article->body !!}</p>
           </article>

           <br>
           <div class="tag">
           <span style="font-size: 1.2rem; font-weight: bold">Tags</span>
            @foreach($article->tags as $tag)
            <a href="/articles/tags/{{$tag->name}}" class="btn waves-effect waves-light btn-small">{{$tag->name}}</a>
            @endforeach
           </div>
		</div>
</div>

<div class="comment-section container">
 
<div class="row">
                @if($message = Session::get('success'))
                    <h4 class="text-accent-4 green-text center-align">{{ $message }}</h4>
                @endif
            </div>
@guest
<h4>Log in to comment.</h4>
@endguest


@auth
        <h4>Add Comment</h4>

 <form method="POST" action="{{url('/comment')}}/{{$article->id}}">
  @csrf
	 <div class="row"> 
		<div class="input-field col s6">
            <textarea id="textarea1" class="materialize-textarea @error('comment') materialize-red lighten-4 @enderror lighten-4" name="comment" ></textarea>
            <label for="textarea1">Add Comment</label>
            @error('comment')
            <span class="helper-text materialize-red-text" data-error="wrong" data-success="right">
            {{$errors->first('comment')}}
            </span>
            @enderror
        </div>

        <div class="input-field col s6">
        <button class="btn waves-effect waves-light" type="submit">Comment
	     </button>

      </div>

</div>  
</form>

@endauth


<br/>

@foreach($article->comments as $comment)

<div class="row">
<div class="col s12 m6 l6 comment">
      <div class="">
        <div class="">
          <span class="uname">{{$comment->user['name']}}</span>
          {{$comment->created_at->toDayDateTimeString()}}
          <p class="comment_text">{{$comment->body}}</p>
        </div>
        <div class="comment_action">
          <button class="btn btn-small btn-flat">
          Reply
         </button>
          <button href="#" class="btn btn-small btn-flat waves-effect waves-red">Report</button>
        </div>
      </div>
    </div>
</div>
@endforeach
</div>



@endsection
