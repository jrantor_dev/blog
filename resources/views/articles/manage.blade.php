@extends('admin_layout')

@section('title','Manage User Articles')

@section('content')

    <div class="content-wrapper">
        <h4>Managing User Articles</h4>

        <div class="content">

            @if($message = Session::get('message'))
                <h2 class="text-success">{{ $message }}</h2>
            @endif

            <table class="table table-bordered">
                <tr>
                    <th>Article ID</th>
                    <th>Author</th>
                    <th>Title</th>
                    <th>Category</th>
                    <th>Excerpt</th>
                    <th>Description</th>
                    <th>Publication Status</th>
                    <th>Action</th>
                </tr>
                @foreach($article_info as $article)
                    <tr>
                        <td>{{ $article->id }}</td>
                        <td>{{$article->name}}</td>
                        <td>{{ $article->title }}</td>
                        <td>{{ $article->category_name }}</td>
                        <td>{{$article->excerpt}}</td>

                        @if(strlen($article->body) > 150)
                        <td>{{\Illuminate\Support\Str::limit($article->body,150, $end ="...") }}</td>
                            @else
                            <td>{{$article->body}}</td>
                        @endif
                        <td>{{ $article->publication_status == 1 ? 'Published' : 'Unpublished' }}</td>
                        <td>
                            @if($article->publication_status == 1)
                                <a href="{{ url('admin/articles/'.$article->id.'/unpublish') }}" class="btn btn-info btn-xs" title="Published">
                                    <i class="fas fa-arrow-up"></i>
                                </a>
                            @else
                                <a href="{{ url('admin/articles/'.$article->id.'/publish') }}" class="btn btn-warning btn-xs" title="Unpublished">
                                    <span class="fas fa-arrow-down"></span>
                                </a>
                            @endif
                            <a href="{{ url('/articles/'.$article->slug) }}" class="btn btn-primary btn-xs" title="Show">
                                <span class="fas fa-search"></span>
                            </a>

{{--                            <a href="{{ url('/articles/'.$article->id.'/delete') }}" onclick="return confirm('Are you sure to delete this !!');" class="btn btn-danger btn-xs" title="Delete">--}}
{{--                                <span class="fas fa-trash"></span>--}}
{{--                            </a>--}}
                        </td>
                    </tr>
                @endforeach
            </table>

        </div>

    </div>

@endsection
