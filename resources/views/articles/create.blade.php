@extends('layout')

@section('title','Create New Post')


@section('content')
<div id="page" class="container">

	<div class="wrapper" >
	<div class="row center-align">
        <h4>New Article</h4>
      <form  action="{{url('/articles')}}" method="POST" class="col s6"  >
        @csrf
        <div class="row">
          <div class="input-field col s12 offset-l5">
            <input id="input_text" class="@error('title') materialize-red lighten-4 @enderror" type="text" name="title" class="validate" value="{{old('title')}}">
            <label for="input_text">Title</label>

            @error('title')
            <span class="helper-text materialize-red-text" data-error="wrong" data-success="right">{{$errors->first('title')}}
            </span>
            @enderror
          </div>
        </div>

          <div class="row">
              <div class="input-field col s12 offset-l5">
                  <input id="input_text" class="@error('slug') materialize-red lighten-4 @enderror" type="text" name="slug" class="validate" value="{{old('slug')}}">
                  <label for="input_text">Slug</label>

                  @error('slug')
                  <span class="helper-text materialize-red-text" data-error="wrong" data-success="right">{{$errors->first('slug')}}
            </span>
                  @enderror
              </div>
          </div>

        <div class="row">
          <div class="input-field col s12 offset-l5">
            <textarea id="textarea1" class="materialize-textarea @error('excerpt') materialize-red lighten-4 @enderror validate" name="excerpt">{{old('excerpt')}}</textarea>
            <label for="textarea1">Excerpt</label>

            @error('excerpt')
            <span class="helper-text materialize-red-text" data-error="wrong" data-success="right">{{$errors->first('excerpt')}}
            </span>
            @enderror
          </div>
        </div>

          <div class="row">
              <div class="input-field col s12 offset-l5">
                  <span>Select Category From Below</span>
                  <select name="category_id" id="categories">
                      <option>Choose</option>
                      @foreach($categories as $category)
                          <option value="{{$category->id}}">{{$category->category_name}}</option>
                      @endforeach
                  </select>

              </div>
          </div>

          <div class="row">
              <div class="input-field col s12 offset-l5">
                  <span>Select Tags From Below</span>
                  <select multiple name="tags[]" id="tags">
                      @foreach($tags as $tag)
                      <option value="{{$tag->id}}">{{$tag->name}}</option>
                      @endforeach
                  </select>

              </div>
          </div>

        <div class="row">
          <div class="input-field col s12 offset-l5">
            <textarea id="textarea2" class="ckeditor materialize-textarea @error('body') materialize-red lighten-4 @enderror validate" name="body" >{{old('body')}}</textarea>
            <label for="textarea2">Description</label>

            @error('body')
            <span class="helper-text materialize-red-text" data-error="wrong" data-success="right">{{$errors->first('body')}}
            </span>
            @enderror
          </div>
        </div>

        <div class="row input-field offset-l5">
         <p> 
         <label for="draft">
           <input class="" type="checkbox" name="draft" id="draft" onclick="saveToggle()"/>
              <span>Save as Draft</span>
          </label>
          </p>
        </div>

      <div class="input-field col s6 offset-l8">
        <button class="btn waves-effect waves-light" id="submit" type="submit">
          Publish 
	     </button>

      </div>

	</form>
	</div>
</div>
</div>
@endsection


@section('scripts')
<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
    <script>
        $(document).ready(function(){
            $('#tags').formSelect();
            $('#categories').formSelect();
        });

        function saveToggle(){
          const sub = document.getElementById('submit');
          let value = sub.getAttribute('value');
          sub.textContent = (sub.textContent ===  'SAVE' ? 'PUBLISH' : 'SAVE');
        }

        $('.ckeditor').ckeditor();

    </script>
@endsection
