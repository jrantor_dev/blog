
<div class="container right-align">
    <h4 class="deep-purple-text">Tags</h4>
        @foreach($tags as $tag)
        <a href="{{url('/articles/tags/'.$tag)}}" class="btn waves-effect waves-light btn-flat">{{$tag}}</a>
        @endforeach
</div>

<div class="container right-align">
    <h4 class="deep-orange-text">Categories</h4>
    <ul>
        @foreach($categories as $category)
            <li>
                <a href="{{url('/articles/categories/'.$category)}}" class="btn waves-effect waves-green btn-small red lighten-1"> {{$category}}</a>
            </li>
            <br>
        @endforeach
    </ul>
</div>

<div class="container right-align">
    <h4 class="materialize-red-text">Archives</h4>
    <ul>
        @foreach($archives as $archive)
            <li>
                <a class="materialize-red-text btn btn-flat" href="?month={{$archive['month']}}&year={{$archive['year']}}">
                    {{$archive['month'].' '.$archive['year'].' ('.$archive['published'].')'}}
                </a>
            </li>
        @endforeach
    </ul>
</div>
