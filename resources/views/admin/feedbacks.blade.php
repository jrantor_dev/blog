@extends('admin_layout')

@section('title','Admin | Contacts')

@section('content')

    <div class="content-wrapper">
        <h4> All Feedbacks</h4>

        <div class="content">

            @if($message = Session::get('message'))
                <h2 class="text-success">{{ $message }}</h2>
            @endif

            @if(!empty($feedbacks->count()))
            <table class="table table-bordered">
                <tr>
                    <th>Feedback ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
                @foreach($feedbacks as $feedback)
                    <tr>
                        <td>{{ $feedback->id }}</td>
                        <td>{{ $feedback->name }}</td>
                        <td>{{ $feedback->email }}</td>

                        @if(strlen($feedback->body) > 150)
                            <td>{{\Illuminate\Support\Str::limit($feedback->body,150, $end ="...") }}</td>
                        @else
                            <td>{{$feedback->body}}</td>
                        @endif

                        <td>

                            <div class="modal fade" id="contact_view_{{$feedback->id}}"  role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="contact_viewLongTitle">From: {{$feedback->name}}</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <h5 id="contact_viewMail">Email: {{$feedback->email}}</h5>
                                            <p id="contact_viewBody"> {{$feedback->body}} </p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="btn btn-primary btn-xs" data-toggle="modal" data-target="#contact_view_{{$feedback->id}}" title="View">
                                <span class="fa fa-search-plus"></span>
                            </a>

                            <a href="{{ url('/contact/'.$feedback->id.'/delete') }}" onclick="return confirm('Are you sure to delete this !!');" class="btn btn-danger btn-xs" title="Delete">
                                <span class="fa fa-trash"></span>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </table>

 @else
     <h3>Here's no feedback for now!</h3>
@endif
 </div>

 </div>
@endsection



