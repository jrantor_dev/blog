@extends('admin_layout')

@section('title','Dashboard')

@section('content')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class=" content container" style="padding-top: 20px;">
                <div class="row">
                    <div class="card w-75 mx-auto">
                        <div class="card-header">
                           Welcome
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">Content Management System for sysadmins!</h5>
                            <p class="card-text">Check sidebar for available actions.</p>
                            <a href="{{url('/admin/stats')}}" class="btn btn-primary">Stats</a>
                        </div>

                        <!-- Small Box (Stat card) -->
                        <h5 class="mb-2 mt-4">Blog Info</h5>
                        <div class="row">
                            <div class="col-lg-3 col-6">
                                <!-- small card -->
                                <div class="small-box bg-info">
                                    <div class="inner">
                                        <h3>{{$new_articles->count()}}</h3>

                                        <p>New Articles</p>
                                    </div>
                                    <div class="icon">
                                        <i class="fas fa-newspaper"></i>
                                    </div>
                                </div>
                            </div>
                            <!-- ./col -->
                            <div class="col-lg-3 col-6">
                                <!-- small card -->
                                <div class="small-box bg-success">
                                    <div class="inner">
                                        <h3>{{$new_users->count()}}</h3>

                                        <p>New Users</p>
                                    </div>
                                    <div class="icon">
                                        <i class="fas fa-user"></i>
                                    </div>
                                </div>
                            </div>
                            <!-- ./col -->
                            <div class="col-lg-3 col-6">
                                <!-- small card -->
                                <div class="small-box bg-warning">
                                    <div class="inner">
                                        <h3>{{$users->count()}}</h3>

                                        <p>Total User</p>
                                    </div>
                                    <div class="icon">
                                        <i class="fas fa-user-plus"></i>
                                    </div>
                                </div>
                            </div>
                            <!-- ./col -->
                            <div class="col-lg-3 col-6">
                                <!-- small card -->
                                <div class="small-box bg-danger">
                                    <div class="inner">
                                        <h3>{{$articles->count()}}</h3>

                                        <p>Total Articles</p>
                                    </div>
                                    <div class="icon">
                                        <i class="fas fa-chart-pie"></i>
                                    </div>
                                    <a href="{{url('admin/articles/manage')}}" class="small-box-footer">
                                        More info <i class="fas fa-arrow-circle-right"></i>
                                    </a>
                                </div>
                            </div>
                            <!-- ./col -->
                        </div>
                    </div>

                </div>
            </div>
        </div>

@endsection

