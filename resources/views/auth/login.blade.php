@extends('layout')

@section('title','Log In')

@section('content')
    <div id="page" class="container">
        <div class="center-align">
            <h3>Log In</h3>

            <div class="container">
                <form  action="{{route('login')}}" method="POST">
                    @csrf

                    <div class="row">
                        <div class="input-field col s6 offset-l3">
                            <input id="email" class="@error('email') materialize-red lighten-4 @enderror" type="email" name="email" class="validate" value="{{old('email')}}">
                            <label for="email">Email</label>

                            @error('email')
                            <span class="helper-text materialize-red-text" data-error="wrong" data-success="right">
                                {{$errors->first('email')}}
                               </span>
                            @enderror
                        </div>
                    </div>


                    <div class="row">
                        <div class="input-field col s6 offset-l3">
                            <input id="password" class="@error('password') materialize-red lighten-4 @enderror" type="password" name="password" class="validate">
                            <label for="password">Password</label>

                            @error('password')
                            <span class="helper-text materialize-red-text" data-error="wrong" data-success="right">
                                {{$errors->first('password')}}
                               </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s6 offset-l1">

                            <p>
                                <label for="remember">
                                    <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} />
                                    <span>Remember Me</span>
                                </label>
                            </p>

                        </div>
                    </div>


                    <div class="input-field col s6">
                        <button class="btn waves-effect waves-light" type="submit">
                            Log In
                        </button>

                        @if (Route::has('password.request'))
                            <a class="btn waves-effect waves-purple" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        @endif

                    </div>

                </form>
            </div>

        </div>

    </div>
@endsection

