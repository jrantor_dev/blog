@extends('layout')

@section('title','Reset Password')

@section('content')
    <div class="container center-align" id="page">

        @if($message = Session::get('status'))
            <h5 class="helper-text green-text">{{ $message }}</h5>
        @endif

        <form method="POST" action="{{route('password.email')}}">
            @csrf
            <div class="row">
                <div class="input-field col s3 offset-l4">
                    <input id="email" class="@error('email') materialize-red lighten-4 @enderror" type="email" name="email" class="validate" value="{{old('email')}}">
                    <label for="email">Email</label>

                    @error('email')
                    <span class="helper-text materialize-red-text" data-error="wrong" data-success="right">
                                {{$errors->first('email')}}
                               </span>
                    @enderror
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6 offset-l2">
                    <button class="btn waves-effect waves-light" type="submit">
                        Send Reset Link
                    </button>
                </div>
            </div>

        </form>
    </div>
@endsection
