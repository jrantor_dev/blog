@extends('layout')

@section('title', 'Reset Password')

@section('content')
    <div class="container center-align" id="page">

        @if($message = Session::get('status'))
            <h5 class="helper-text green-text">{{ $message }}</h5>
        @endif

        <form method="POST" action="{{route('password.update')}}">
            @csrf
            <input type="hidden" name="token" value="{{$token}}">
            <div class="row">
                <div class="input-field col s6 offset-l3">
                    <input id="email" class="@error('email') materialize-red lighten-4 @enderror" type="email" name="email" class="validate" value="{{old('email')}}">
                    <label for="email">Email</label>

                    @error('email')
                    <span class="helper-text materialize-red-text" data-error="wrong" data-success="right">
                                {{$errors->first('email')}}
                               </span>
                    @enderror
                </div>
            </div>

            <div class="row">
                <div class="input-field col s6 offset-l3">
                    <input id="password" class="@error('password') materialize-red lighten-4 @enderror" type="password" name="password" class="validate">
                    <label for="password">Password</label>

                    @error('password')
                    <span class="helper-text materialize-red-text" data-error="wrong" data-success="right">
                                {{$errors->first('password')}}
                               </span>
                    @enderror
                </div>
            </div>

            <div class="row">
                <div class="input-field col s6 offset-l3">
                    <input id="password-confirm"  type="password" name="password_confirmation" class="validate">
                    <label for="password-confirm">Confirm Password</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s6 offset-l3">
                    <button class="btn waves-effect waves-light" type="submit">
                        Reset Password
                    </button>
                </div>
            </div>

        </form>
    </div>
@endsection
