@extends('layout')

@section('title','Register')

@section('content')
    <div id="page" class="container">
        <div class="center-align">
            <h3>Register</h3>

            <div class="container">
                <form  action="{{route('register')}}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="input-field col s6 offset-l3">
                            <input id="name" class="@error('name') materialize-red lighten-2 @enderror" type="text" name="name" class="validate" value="{{old('name')}}">
                            <label for="name">Name</label>

                            @error('name')
                               <span class="text-bold materialize-red lighten-3" data-error="wrong" data-success="right">
                                {{$errors->first('name')}}
                               </span>
                            @enderror
                        </div>
                    </div>


                    <div class="row">
                        <div class="input-field col s6 offset-l3">
                            <input id="email" class="@error('email') materialize-red lighten-2 @enderror" type="email" name="email" class="validate" value="{{old('email')}}">
                            <label for="email">Email</label>

                            @error('email')
                            <span class="text-bold materialize-red lighten-3" data-error="wrong" data-success="right">
                                {{$errors->first('email')}}
                               </span>
                            @enderror
                        </div>
                    </div>


                    <div class="row">
                        <div class="input-field col s6 offset-l3">
                            <input id="password" class="@error('password') materialize-red lighten-2 @enderror" type="password" name="password" class="validate">
                            <label for="password">Password</label>

                            @error('password')
                            <span class="text-bold  materialize-red lighten-3" data-error="wrong" data-success="right">
                                {{$errors->first('password')}}
                               </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s6 offset-l3">
                            <input id="password-confirm"  type="password" name="password_confirmation" class="validate">
                            <label for="password-confirm">Confirm Password</label>
                        </div>
                    </div>


                    <div class="input-field col s6">
                        <button class="btn waves-effect waves-light" type="submit">Register
                        </button>

                    </div>

                </form>
            </div>

        </div>

    </div>
@endsection

