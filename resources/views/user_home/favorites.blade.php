@extends('layout')

@section('title','Dashboard | Drafts')

@section('content')
    <div id="page" class="container center-align">
    <h2>Author Dashboard</h2>
        <div class="col s12 m6">
            <div class="card">
                <div class="card-title">
                    
                </div>
                <div class="card-content">
                <img src="../images/user.jpg">
                    <span class="card-title">{{auth()->user()->name}}</span>
                    <h5></h5>
                </div>
                <div class="card-action">
                    <a href="{{url('articles/create')}}">
                           Create Article
                    </a>
                    <a href="{{url('tags/create')}}" class="">
                        Create Tag
                    </a>
                    <a href="{{url('/favorites')}}" class="nav_links">
                        Favorites
                    </a>
                    <a href="{{url('/drafts')}}" class="">
                        Drafts
                    </a>
                    <a href="{{url('/profile')}}" class="">
                        Profile
                    </a>
                    <a href="#" class="">
                        Activity
                    </a>     
                </div>
            </div>
        </div>
        <div class="row" style="padding-top: 16px;">
            <h4>Favorite Posts</h4>
            <div class="container col s12 m12 l12">
                @if(sizeof($favorites) > 0)
                    @foreach($favorites as $article)
                        <div class="card medium sticky-action col s6 m4 l3" style="padding: 10px; margin: 10px;">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator" src="{{asset('images/office.jpg')}}">
                            </div>
                            <div class="card-content">
                                <span class="card-title activator grey-text text-darken-4">
                                    {{$article->title}}
                                </span>
                                <p> {{Carbon\Carbon::parse($article->created_at)->diffForHumans()}}</p>
                            </div>
                            <div class="card-action">
                               <a class="" href="{{url('articles/'.$article->slug)}}" >
                                    <i class="material-icons">visibility</i>
                                </a>
                                <a class="btn btn-small materialize-red" href="{{url('/article/rm_favorite/'.$article->id)}}" onclick="return confirm('Remove from Favorites?');" >
                                    <i class="material-icons">thumb_down</i>
                                </a>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">{{$article->title}}<i class="material-icons right">close</i></span>
                                <p>{{$article->excerpt}}</p>
                            </div>
                        </div>
                    @endforeach
                @else
                        <h5 class="center-align">There's no article favorited by you.</h5>
                @endif
            </div>
        </div>
    </div>
@endsection

