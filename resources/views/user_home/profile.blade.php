@extends('layout')

@section('title','Profile')

@section('content')
<div id="page" class="container center-align">
        <div class="col s12 m6">
            <div class="card">
                <div class="card-title">
                    Author Dashboard
                </div>
                <div class="card-content">
                <img src="../images/user.jpg">
                    <span class="card-title">{{auth()->user()->name}}</span>
                    <h5></h5>
                </div>
                <div class="card-action">
                    <a href="{{url('articles/create')}}">
                           Create Article
                    </a>
                    <a href="{{url('tags/create')}}" class="">
                        Create Tag
                    </a>
                    <a href="{{url('/favorites')}}" class="">
                        Favorites
                    </a>
                    <a href="{{url('/drafts')}}" class="">
                        Drafts
                    </a>
                    <a href="{{url('/profile')}}" class="nav_links">
                        Profile
                    </a>
                    <a href="#" class="">
                        Activity
                    </a>     
                </div>
            </div>
        </div>
        <div class="row" style="padding-top: 16px;">
            <h4> Profile </h4>
            <div class="container col s12 m12 l12">
            </div>
        </div>
    </div>
@endsection