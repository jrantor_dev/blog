@extends('layout')

@section('title' ,'HOME')

@section('header')
<div id="header-featured">
        <div id="banner-wrapper">
            <div id="banner" class="container">
                <h2>Welcome to SimpleBlog</h2>
                <p>This is <strong>SimpleBlog</strong>, a free, fully standards-compliant web platform developed by me. </p>

                @if(auth()->user())
                <a href="{{url('home')}}" class="button">Dashboard</a>
                @else
                <a href="{{url('login')}}" class="button">Log In</a>
                @endif

            </div>
        </div>
</div>
@endsection

@section('content')

@endsection
