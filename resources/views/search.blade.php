@extends('layout')

@section('title','SimpleBlog | Search')

@section('content')
<div id="page" class="center-align">
    <form action="{{route('search')}}" method="GET">
        <div class="row">
            <div class="input-field col s12 m6 offset-l3">
                <input id="input_text" class="@error('search') materialize-red lighten-4 @enderror" type="text" name="search" class="validate" value="{{old('search')}}">
                <label for="input_text">Search</label>

                @error('search')
                <span class="helper-text materialize-red-text" data-error="wrong" data-success="right">{{$errors->first('search')}}
            </span>
                @enderror
            </div>
        </div>

        <div class="row">
            <div class="input-field col s9 offset-s1">
                <button class="btn waves-effect waves-light" type="submit">Search
                </button>
            </div>
        </div>

    </form>

</div>
@endsection
