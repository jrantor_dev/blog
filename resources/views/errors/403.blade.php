@extends('layout')

@section('title', 'Access Denied')

@section('content')
<div class="container center-align" id="page">
    <h5 class="teal-text text-darken-2"> 403 | {{$exception->getMessage()}}</h5>
</div>
@endsection
