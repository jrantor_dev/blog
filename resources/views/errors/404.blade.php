@extends('layout')

@section('title', 'Not Found')

@section('content')
    <div class="container center-align" id="page">
        <h5 class="teal-text text-darken-2"> 404 | Looks like you are lost!</h5>
    </div>
@endsection
