<!DOCTYPE html>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name=”viewport” content=”width=device-width”>
<title>@yield('title')- SimpleBlog</title>
<link rel="icon" href="{{asset('images/favicon.ico')}}" >
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet" />

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
<link href="{{asset('css/materialize.css')}}" rel="stylesheet"/>
<link href="{{asset('css/default.css')}}" rel="stylesheet"/>
<link href="{{asset('css/fonts.css')}}" rel="stylesheet"/>

@yield('links')
<!--[if IE 6]><link href="default_ie6.css" rel="stylesheet" type="text/css" /><![endif]-->

</head>
<body>
<div id="header-wrapper">
	<div id="header" class="container">
		<div id="logo">
			<h1><a href="{{url('/')}}">SimpleBlog</a></h1>
		</div>
		<div id="menu">
			<ul>
				<li class="{{Request::path()=== '/' ? 'current_page_item' : ''}}"><a href="{{url('/home')}}" accesskey="1" title="">Homepage</a></li>
				<li class="{{Request::path()=== 'articles' ? 'current_page_item' : ''}}"><a href="{{url('/articles')}}" accesskey="4" title="">Articles</a></li>
                @if(Auth::check())
                <li >
                    <a href="{{route('user.logout')}}">Logout</a>

{{--                    <a class="dropdown-item" href="{{ route('logout') }}"--}}
{{--                            onclick="event.preventDefault();--}}
{{--                                                     document.getElementById('logout-form').submit();">--}}
{{--                        {{ __('Logout') }}--}}
{{--                    </a>--}}

{{--                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
{{--                        @csrf--}}
{{--                    </form>--}}

                </li>
                    <li class="{{Request::path()=== 'tags' ? 'current_page_item' : ''}}"><a href="{{url('tags')}}" accesskey="3" title="">Tags</a></li>
                    <li class="{{Request::path()=== 'articles/create' ? 'current_page_item' : ''}}"><a href="{{url('/articles/create')}}" accesskey="2" title="">Create</a></li>
                @else
                    <li class="{{Request::path()=== 'login' ? 'current_page_item' : ''}}"><a href="{{url('login')}}" accesskey="5" title="">Login</a></li>

                    <li class="{{Request::path()=== 'register' ? 'current_page_item' : ''}}"><a href="{{url('register')}}" accesskey="5" title="">Register</a></li>
                @endif

                <li class="{{Request::path()=== 'find' ? 'current_page_item' : ''}}"><a href="{{url('find')}}" accesskey="5" title="">Search</a></li>

                <li class="{{Request::path()=== 'about' ? 'current_page_item' : ''}}"><a href="{{url('about')}}" accesskey="3" title="">About Us</a></li>
				<li class="{{Request::path()=== 'contact' ? 'current_page_item' : ''}}"><a href="{{url('contact')}}" accesskey="5" title="">Contact Us</a></li>


            </ul>
		</div>
	</div>
<div class="col s12 m6 l3">
    @yield('header')
</div>
</div>
    @yield('content')

<div id="copyright" class="col s12 m6 l3">
	<p>&copy; {{date('Y')}} Antor Roy. | Photos by <a href="http://fotogrph.com/">Fotogrph</a> | Inspired by <a href="http://templated.co" rel="nofollow">TEMPLATED</a>.
        <a href="#" class="btn waves-light btn-sm">
            <i class="material-icons">arrow_upward</i>
        </a>
    </p>
</div>

<script type="text/javascript" src="{{asset('plugins/jquery/jquery.js')}}"></script>
<script type="text/javascript" src="{{asset('js/materialize.min.js')}}" ></script>

@yield('scripts')

</body>
</html>
