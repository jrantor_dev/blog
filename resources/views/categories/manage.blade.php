@extends('admin_layout')

@section('title','Manage Categories')

@section('content')

    <div class="content-wrapper">
        <h4> Manage Categories</h4>

        <div class="content">

            @if($message = Session::get('message'))
                <h2 class="text-success">{{ $message }}</h2>
            @endif

            <table class="table table-bordered">
                <tr>
                    <th>Category ID</th>
                    <th>Category Name</th>
                    <th>Category Description</th>
                    <th>Publication Status</th>
                    <th>Action</th>
                </tr>
                <?php $i = 1; ?>
                @foreach($categories as $category)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $category->category_name }}</td>
                        <td>{{ $category->description }}</td>
                        <td>{{ $category->publication_status == 1 ? 'Published' : 'Unpublished' }}</td>
                        <td>
                            @if($category->publication_status == 1)
                                <a href="{{ url('admin/categories/'.$category->category_name.'/unpublish') }}" class="btn btn-info btn-xs" title="Published">
                                    <i class="fas fa-arrow-up"></i>
                                </a>
                            @else
                                <a href="{{ url('admin/categories/'.$category->category_name.'/publish') }}" class="btn btn-warning btn-xs" title="Unpublished">
                                    <span class="fas fa-arrow-down"></span>
                                </a>
                            @endif
                            <a href="{{ url('admin/categories/'.$category->category_name.'/edit') }}" class="btn btn-primary btn-xs" title="Edit">
                                <span class="fas fa-edit"></span>
                            </a>
                            <a href="{{ url('admin/categories/'.$category->category_name.'/delete') }}" onclick="return confirm('Are you sure to delete this !!');" class="btn btn-danger btn-xs" title="Delete">
                                <span class="fas fa-trash"></span>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </table>

        </div>

    </div>

@endsection
