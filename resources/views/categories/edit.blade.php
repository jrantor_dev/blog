@extends('admin_layout')

@section('title','Create Category')

@section('content')

    <div class="content-wrapper">

        <h4>Update a category.</h4>
        <div class="content container col-sm-10" style="padding-top: 10px;">
            @if($message = Session::get('message'))
                <h2 class="text-success">{{ $message }}</h2>
            @endif
            <form class="form-horizontal" action="{{ url('/admin/categories/'.$category->category_name) }}" method="POST">
                @csrf
                @method('put')
                <div class="form-group">
                    <label class="col-sm-3">Category Name</label>
                    <div class="col-sm-9">
                        <input type="text" name="category_name" value="{{$category->category_name}}" class="form-control {{$errors->has('category_name') ? 'is-invalid' : ''}}" />

                        @if ($errors->has('category_name'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('category_name') }}</strong>
                            </span>
                        @endif

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3">Category Description</label>
                    <div class="col-sm-9">
                        <textarea class="form-control {{$errors->has('description') ? 'is-invalid' :''}}" name="description">{{$category->description}}</textarea>

                        @if ($errors->has('description'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3">Publication Status</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="publication_status">
                            @if($category->publication_status == 1)
                                <option value="1" selected>Published</option>
                                <option value="0">Unpublished</option>
                            @else
                                <option value="1">Published</option>
                                <option value="0" selected>Unpublished</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-success btn-block">
                            Submit
                        </button>

                    </div>
                </div>
            </form>

        </div>

    </div>


@endsection
