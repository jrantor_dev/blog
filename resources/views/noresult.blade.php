@extends('layout')

@section('title','No Result')

@section('content')
    <div class="container center-align" id="page">
        <i class="material-icons">search</i>
        <h5 class="teal-text text-darken-2"> Nothing Found!</h5>
        <a href="{{url('find')}}"> <button class="btn btn-small">Search Again</button> </a>
    </div>
@endsection
