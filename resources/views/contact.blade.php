@extends('layout')

@section('title', 'Contact Us')

@section('content')
    <div id="page" class="container">
        <div class="center-align">

            @if($message = Session::get('message'))
                <h5 class="helper-text green-text">{{ $message }}</h5>
            @endif

            <h3>Contact</h3>

            <div class="container">
                <form  action="{{url('/contact')}}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="input-field col s6">
                            <input id="input_text" class="@error('name') materialize-red lighten-4 @enderror" type="text" name="name" class="validate" value="{{old('name')}}">
                            <label for="input_text">Name</label>

                            @error('name')
                            <span class="helper-text materialize-red-text" data-error="wrong" data-success="right">
                                {{$errors->first('name')}}
                            </span>
                            @enderror
                        </div>

                        <div class="input-field col s6">
                            <textarea id="email" type="email" class="materialize-textarea @error('email') materialize-red lighten-4 @enderror validate" name="email">{{old('email')}}</textarea>
                            <label for="email">Email</label>

                            @error('email')
                            <span class="helper-text materialize-red-text" data-error="wrong" data-success="right">
                                {{$errors->first('email')}}
                            </span>
                            @enderror
                        </div>
                    </div>


                    <div class="row">
                        <div class="input-field col s12">
                            <textarea id="body" class="materialize-textarea @error('body') materialize-red lighten-4 @enderror validate" name="body" >{{old('body')}}</textarea>
                            <label for="body">Description</label>

                            @error('body')
                            <span class="helper-text materialize-red-text" data-error="wrong" data-success="right">
                                {{$errors->first('body')}}
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="input-field col s6">
                        <button class="btn waves-effect waves-light" type="submit">Send
                            <i class="material-icons right">send</i>
                        </button>

                    </div>

                </form>
            </div>

        </div>

    </div>
@endsection
