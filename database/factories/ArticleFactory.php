<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Article;
use Faker\Generator as Faker;

$factory->define(Article::class, function (Faker $faker) {
    return [
        'user_id' => function(){
           return factory(App\User::class)->create()->id;
        },
        'category_id' => function(){
           return factory(App\Category::class)->create()->id;
        },
        'title' => $faker->title,
        'slug' => $faker->unique()->slug,
        'excerpt' =>$faker->text,
        'body' => $faker->text,
        'publication_status' => 1
    ];
});
