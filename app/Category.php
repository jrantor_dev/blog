<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['category_name', 'description', 'publication_status'];

    public function admin(){
        return $this->belongsTo(Admin::class);
    }

    public function articles(){
        return $this->hasMany(Article::class);
    }

    public function getRouteKeyName()
    {
        return 'category_name';
    }


}
