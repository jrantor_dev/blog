<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin')->except('articlesByCategory');
    }



    public function create()
    {
        return view('categories.create');
    }

    public function edit(Category $category)
    {
        return view('categories.edit',compact('category'));

    }

    public function store(Request $request){

        $this->validateCategory();

        $category = new Category;

        $category->category_name = $request->category_name;
        $category->description = $request->description;
        $category->publication_status = $request->publication_status;
        $category->save();

        return redirect('/admin/categories/create')->with('message','Category Info Saved Successfully');

    }


    public function update(Category $category){
        $this->validateCategory();

        $category->category_name = \request('category_name');
        $category->description = \request('description');
        $category->publication_status = \request('publication_status');
        $category->save();

        return redirect('/admin/categories/manage')->with('message','Category Info Updated Successfully');
    }

    public function destroy(Category $category){
        $category->delete();

        return redirect('/admin/categories/manage')->with('message','Category Info Deleted Successfully');
    }

    public function manageCategory(){
        $categories = Category::all();
        return view('categories.manage', compact('categories'));
    }

    public function publish_category(Category $category){
        $category->publication_status = 1;
        $category->save();

        return redirect('/admin/categories/manage')->with('message','Category Published Successfully');
    }

    public function unpublish_category(Category $category){
        $category->publication_status = 0;
        $category->save();

        return redirect('/admin/categories/manage')->with('message','Category Unpublished Successfully');

    }

     public function articlesByCategory(Category $category){
        $articles = $category->articles()->where('publication_status','=','1')->latest();

         if($month = request('month')){
             $articles->whereMonth('created_at', Carbon::parse($month)->month);
         }

         if($year = request('year')){
             $articles->whereYear('created_at', $year);
         }
         $articles = $articles->simplePaginate(3);

         $archives = Article::selectRaw("extract(year from created_at) as year,
            to_char(created_at,'Month') as month, count(*) as published")
             ->where('publication_status','=','1')
             ->groupBy('year','month')
             ->orderByRaw('min(created_at) desc')
             ->get()
             ->toArray();

        if($articles->count() <= 0){
            return view('noresult');
        }

        else{
            return view('articles.all',['articles'=>$articles ,'archives'=>$archives]);
        }

     }

    protected function validateCategory(){
        return request()->validate([
            'category_name' => 'required | max:100',
            'description' => 'required | max:255',
            'publication_status' => 'required'
        ]);
    }

}
