<?php

namespace App\Http\Controllers;

use App\Article;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $users = User::all();
        $articles = Article::where('publication_status', '=', '1');
        $new_articles = Article::where('created_at', '>=', Carbon::now()->subDay())->get();
        $new_users = User::where('created_at', '>=', Carbon::now()->subDay())->get();
        return view('admin.home',compact('articles','users','new_articles','new_users'));
    }

    public function logout(){
        Auth::logout();
    }

    public function show_stats(){
        return view('admin.stats');
    }

}
