<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Favorite;
use Session;
use DB;

class FavoriteController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $user = auth()->user()->id;
        
        $favorites = DB::table('articles')
                    ->join('favorites','articles.id','=','favorites.article_id')
                    ->select('articles.*')
                    ->where('favorites.user_id','=',$user)
                    ->orderBy('favorites.created_at','desc')
                    ->get();
        return view('user_home.favorites',['favorites'=>$favorites]);

    }

    public static function show(Article $article){
        return $article->favorites->count();
    }

    public function add_to_fav(Article $article){
        $user = \auth()->user()->id;
        $fav = new Favorite;
        $fav->user_id = $user;
        $fav->article_id = $article->id;
        $fav->save();

        Session::flash('success','Article added to favorites');
        return redirect('/articles');
    }

    public function rm_from_fav(Article $article){
        $user = auth()->user();

        $user->favorites()->where('article_id',$article->id)->delete();
        Session::flash("success","Removed from favorites");
        return redirect("/articles");
    }
}
