<?php

namespace App\Http\Controllers;
use App\Article;
use App\Category;
use App\Tag;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
class ArticleController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth')
        ->except('index','show','manageArticle','search');
    }

    public function index(){
    	$articles = Article::where('publication_status','=','1')->latest();

    	if($month = request('month')){
    	    $articles->whereMonth('created_at', Carbon::parse($month)->month);
        }

        if($year = request('year')){
            $articles->whereYear('created_at', $year);
        }
        $articles = $articles->simplePaginate(4);

    	$archives = Article::selectRaw("extract(year from created_at) as year,
            DATE_FORMAT(created_at,'%M') as month, count(*) as published")
            ->where('publication_status','=','1')
            ->groupBy('year','month')
            ->orderByRaw('min(created_at) DESC')
            ->get()
            ->toArray();
//    	$categories = Category::where('publication_status','=','1')->orderBy('category_name','asc')->get();
//    	$tags = Tag:: latest()->get();

    	return view('articles.all',['articles'=> $articles, 'archives'=>$archives]);
    }

      public function show($slug){
        $article = Article::where('slug','=',$slug)->where('publication_status','=','1')->first();
        
        if(empty($article)){
            abort(403,'Maybe the article does not exist yet!');
        }


        else{
            $author = User::find($article->user_id);
            $category = Category::find($article->category_id);
            return view('articles.show', ['article' => $article, 'author'=>$author, 'category'=>$category]);
        }

 }
    public function create(){
        $tags = DB::table('tags')->orderBy('id')->get();
        $categories = DB::table('categories')->where('publication_status','=','1')->orderBy('id')->get();
    	return view('articles.create',compact('tags','categories'));

    }

    public function store(Request $request){

        //long code
        $this->validateArticle();

        $article = new Article;
        $article->title = $request->title;
        $article->slug = $request->slug;
        $article->excerpt = $request->excerpt;
        $article->category_id = $request->category_id;
        $article->user_id = auth()->user()->id;
        $article->body = $request->body;
        if($request->exists('draft')){
            $article->publication_status = 0;
        }

        $article->save();

        $article->tags()->sync($request->tags,false);

        Session::flash("success", "Article Saved Successfully");

       // Article::create($this->validateArticle());

        return redirect('/articles');
    }


    public function edit($slug){
        $article = Article::where('slug','=', $slug)->first();
        $tags = Tag::latest()->get();
        $categories = Category::where('publication_status','=','1')->orderBy('id','desc')->get();
        if(auth()->user()->id == $article->user_id){
            return view('articles.edit',['article'=>$article,'tags'=>$tags,'categories'=>$categories]);
        }

        else{
            return abort(403,'Access denied!');
        }
    }

    public function update(Article $article){

        if(\request('slug') == $article->slug){
            request()->validate([
                'title' => 'required',
                'excerpt' => 'required',
                'body' => 'required'
            ]);
        }

        else{
            $this->validateArticle();
        }

        //long code
        $article->title = \request('title');
        $article->slug = \request('slug');
        $article->excerpt = \request('excerpt');
        $article->category_id = \request('category_id');
        $article->user_id = auth()->user()->id;
        $article->body = \request('body');

        $article->save();

        $article->tags()->sync(\request('tags'));

        Session::flash("success", "Article Updated!");

        return redirect('/articles/' . $article->slug);
    }


    protected function validateArticle(){

        return request()->validate([

            'title' => 'required',
            'slug'  => 'required|alpha_dash|unique:articles,slug',
            'excerpt' => 'required',
            'body' => 'required'
        ]);
    }

    public function destroy(Article $article){

        $article->delete();
        return redirect('/articles')->with('message','Article Deleted!');
    }

    public function manageArticle(){

        $article_info = DB::table('articles')
                        ->join('categories','articles.category_id', '=', 'categories.id' )
                        ->join('users', 'articles.user_id', '=', 'users.id')
                        ->select('articles.id','articles.slug','articles.title','articles.body','articles.excerpt','articles.publication_status','categories.category_name', 'users.name')
                        ->get();
        return view('articles.manage', compact('article_info'));
    }


    public function publish(Article $article){
        $article->publication_status = 1;
        $article->save();

        return redirect('/admin/articles/manage')->with('message','Article Published Successfully');
    }

    public function unpublish(Article $article){
        $article->publication_status = 0;
        $article->save();

        return redirect('/admin/articles/manage')->with('message','Article Unpublished Successfully');
    }

    public function search(Request $request){

        $request->validate(
            ['search' => 'required']
        );

        $search = $request->input('search');

        $articles = Article::where('publication_status','=','1')
                    ->where('title','LIKE','%'.$search.'%')
                    ->orWhere('body','LIKE','%'.$search.'%')
                    ->latest();

        if($month = request('month')){
            $articles->whereMonth('created_at', Carbon::parse($month)->month);
        }

        if($year = request('year')){
            $articles->whereYear('created_at', $year);
        }
        $articles = $articles->simplePaginate(3);

        $archives = Article::selectRaw("extract(year from created_at) as year,
        DATE_FORMAT(created_at,'Month') as month, count(*) as published")
            ->where('publication_status','=','1')
            ->groupBy('year','month')
            ->orderByRaw('min(created_at) DESC')
            ->get()
            ->toArray();

        if($articles->count() <= 0){
            return view('noresult');
        }

        else{
            return view('articles.all', compact('articles','archives'));
        }

    }

}
