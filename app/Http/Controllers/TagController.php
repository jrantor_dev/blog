<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use App\Tag;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TagController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')
            ->except('show');
    }

    public function index(){

        $tags = Tag::latest()->paginate(5);
        return view('tags.all',['tags'=>$tags]);
    }

    public function show(Tag $tag){
        $articles = $tag->articles()->where('publication_status','=','1')->latest();

        if($month = request('month')){
            $articles->whereMonth('created_at', Carbon::parse($month)->month);
        }

        if($year = request('year')){
            $articles->whereYear('created_at', $year);
        }
        $articles = $articles->simplePaginate(3);

        $archives = Article::selectRaw("extract(year from created_at) as year,
            to_char(created_at,'Month') as month, count(*) as published")
            ->where('publication_status','=','1')
            ->groupBy('year','month')
            ->orderByRaw('min(created_at) desc')
            ->get()
            ->toArray();

//        $categories = Category::where('publication_status','=','1')->orderBy('category_name','asc')->get();
//        $tags = Tag::latest()->get();
        if($articles->count() <= 0){
            return view('noresult');
        }
        else{
            return view('articles.all',compact('articles','archives'));
        }
    }

    public function create(){
        return view('tags.create_tag');
    }

    public function store(Request $request){
        $this->validateTag();
        $tag = new Tag;
        $tag->name = $request->name;
        $tag->user_id = auth()->user()->id;
        $tag->save();

        return redirect('/tags/create')->with('message', "Tag Saved Successfully");
    }

    public function edit(Tag $tag){

        if(auth()->user()->id == $tag->user_id){
            return view('tags.edit', compact("tag"));
        }

        else{
            abort(403,'No permission to do edit.');
        }
    }

    public function update(Tag $tag){
        $tag->update($this->validateTag());
        return redirect('/tags')->with('message','Tag updated');
    }

    public function destroy(Tag $tag){
        $tag->delete();
        return redirect('/tags')->with('message','Tag deleted');
    }

    protected function validateTag(){

        return request()->validate([

            'name' => 'required|unique:tags,name'
        ]);
    }


}
