<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')
        ->except('feedback');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user_articles =  Article::where([
               ['user_id', '=', \auth()->user()->id],
               ['publication_status', '=', '1'],
            ])
            ->orderBy('id','desc')
            ->simplePaginate(6);
        return view('home',compact('user_articles'));
    }

    public function show_drafts(){
        $user_drafts =  Article::where([
            ['user_id', '=', \auth()->user()->id],
            ['publication_status', '=', '0'],
         ])
         ->orderBy('id','desc')
         ->simplePaginate(6);
     return view('user_home.drafts',['user_drafts'=>$user_drafts]);
    }

    public function profile(){
        return view('user_home.profile');
    }
}
