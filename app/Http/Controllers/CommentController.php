<?php

namespace App\Http\Controllers;
use App\Comment;
use Illuminate\Http\Request;
use App\Article;
use Session;
class CommentController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function store(Request $request,$article_id){
        request()->validate([
            'comment' => 'required|max:300'
        ]);

        $user_id = \auth()->user()->id;
        $article = Article::find($article_id);
        $comment = new Comment;
        $comment->user_id = $user_id;
        $comment->body = $request->comment;
        $comment->article()->associate($article);

        $comment->save();

        Session::flash('success','Comment added successfully.');

        return redirect()->route('single.article',[$article->slug]);


    }
}
