<?php

namespace App\Http\Controllers;
use App\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin')
            ->except('store');
    }

    public function index(){
        $feedbacks = Contact::all();

        return view('admin.feedbacks', compact('feedbacks'));
    }

    public function store(){

        request()->validate([
            'name' => 'required | max:100',
            'email' => 'required | email',
            'body' => 'required'
        ]);


        $contact = new Contact;

        $contact->name = request('name');
        $contact->email = request('email');
        $contact->body = request('body');
        $contact->save();

        return redirect('/contact')->with('message', 'Feedback Sent.');
    }

    public function destroy(Contact $contact){
        $contact->delete();

        return redirect('/contact/all')->with('Message','Feedback Deleted');
    }

}
