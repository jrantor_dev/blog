<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminLoginController extends Controller
{
    public function __construct(){
        $this->middleware('guest:admin')->except('logout');
    }

    public function showLoginForm(){
        return view('admin.login');
    }

    public function login(Request $request){
        //validate the form data
        $this->validate($request,[
           'email' => 'required|email',
           'password' => 'required'
        ]);

        //attempt login
       if (Auth::guard('admin')->attempt(['email'=>$request->email,
           'password'=>$request->password
           ], $request->filled('remember')))
       {
           return redirect()->intended('admin/home');
       }
        //rdirect to intended location
        //unsuccessful go back

        return redirect()->back()->withInput($request->only('email'))->with('message','Sorry! Credentials do not match!');
    }

    public function logout()
    {
        Auth::guard('admin')->logout();

        return redirect('/admin/login');
    }

}
