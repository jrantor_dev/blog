<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use function foo\func;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('articles.sidebar', function($view){
           $view->with('tags', \App\Tag::pluck('name'));
            $view->with('categories', \App\Category::pluck('category_name'));
        });

        If (env('APP_ENV') == 'production') {
            $this->app['request']->server->set('HTTPS', true);
        }
    }
}
