<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = ['title', 'excerpt', 'body'];

    public function user(){
    	return $this->belongsTo(User::class);
    }

    public function tags(){

    	return $this->belongsToMany(Tag::class,'article_tag');
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function comments(){
        return $this->hasMany(Comment::class);
    }

    public function favorites(){
        return $this->hasMany(Favorite::class);
    }

    public static function isFavorite(Article $article){
        $user = \auth()->user();

        return $user->favorites()->where('article_id',$article->id)->exists();
    }
}
