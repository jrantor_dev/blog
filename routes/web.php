<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// user dashboard routes
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/drafts','HomeController@show_drafts')->name('drafts');
Route::get('/favorites','FavoriteController@index');
Route::get('/profile', 'HomeController@profile');

Route::get('/about', function () {

    return view('about');
});

//contact_form_handlers

Route::get('/contact',function (){
    return view('contact');
});

Route::post('/contact', 'ContactController@store');
Route::get('/contact/all', 'ContactController@index');
Route::get('/contact/{contact}/delete','ContactController@destroy');


//search route

Route::get('/find', function(){
    return view('search');
});
Route::get('/search','ArticleController@search')->name('search');

//article routes

Route::get('/articles','ArticleController@index');
Route::post('/articles','ArticleController@store');
Route::get('/articles/create','ArticleController@create');
Route::get('/articles/{slug}','ArticleController@show')->name('single.article')->where('slug','[\w\d\-\_]+');
Route::get('/articles/{slug}/edit','ArticleController@edit')->where('slug','[\w\d\-\_]+');
Route::put('/articles/{article}','ArticleController@update');
Route::get('/articles/{article}/delete','ArticleController@destroy');

Route::get('/articles/categories/{category}','CategoryController@articlesByCategory');

// comment routes
Route::post('/comment/{id}','CommentController@store');

// favorite routes
Route::get('favorite/articles/{user}','FavoriteController@show');
Route::post('/article/favorite/{article}', 'FavoriteController@add_to_fav');
Route::get('/article/rm_favorite/{article}','FavoriteController@rm_from_fav');

//tag routes
Route::get('/tags/create','TagController@create');
Route::get('/tags','TagController@index');
Route::post('/tags','TagController@store');
Route::get('tags/edit/{tag}','TagController@edit');
Route::put('/tags/{tag}','TagController@update');
Route::get('/tags/{tag}/delete','TagController@destroy');
Route::get('/articles/tags/{tag}','TagController@show');

Auth::routes();

Route::get('/user/logout', 'Auth\LoginController@userLogout')->name('user.logout');

//admin routes

Route::get('/admin/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
Route::post('/admin/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
Route::get('/admin/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
Route::get('/admin/home', 'AdminController@index');

//category action routes

//Route::get('/admin/categories','CategoryController@index');
Route::post('/admin/categories','CategoryController@store');
Route::get('/admin/categories/create','CategoryController@create')->name('categories.create');
Route::get('/admin/categories/{category}/edit','CategoryController@edit');
Route::put('/admin/categories/{category}','CategoryController@update');
Route::get('/admin/categories/{category}/delete','CategoryController@destroy');
Route::get('/admin/categories/{category}/publish','CategoryController@publish_category');
Route::get('/admin/categories/{category}/unpublish','CategoryController@unpublish_category');

//admin manage routes
Route::get('/admin/categories/manage','CategoryController@manageCategory');
Route::get('/admin/articles/manage','ArticleController@manageArticle')->middleware('auth:admin');
Route::get('/admin/articles/{article}/publish','ArticleController@publish')->middleware('auth:admin');
Route::get('/admin/articles/{article}/unpublish','ArticleController@unpublish')->middleware('auth:admin');

///admin stats
Route::get('/admin/stats','AdminController@show_stats');


//admin password resets
/*
Route::post('/admin-password/email', 'Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
Route::get('/admin-password/reset', 'Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
Route::post('/admin-password/reset', 'Admin\ResetPasswordController@reset');
Route::get('/admin-password/reset/{token?}', 'Admin\ResetPasswordController@showResetForm')->name('admin.password.reset');

*/
